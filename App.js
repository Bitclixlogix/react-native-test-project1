import React, { Component } from 'react';
import { SafeAreaView, View } from 'react-native';

// App route initialisation
import AppNavigation from './source/navigations';

//resource
import { Style2 } from './source/resource/commonStyles';

export default class App extends Component {
  render() {
    return (
      <View style={Style2.flex}>
        <AppNavigation />
      </View>
    )
  }
}