// Libraries
import React from 'react';
import { Image } from 'react-native';

import {
    createAppContainer,
    createStackNavigator,
    createBottomTabNavigator
} from 'react-navigation';

//App Screens
import Favourites from '../screens/TabScreens/favourites';
import Featured from '../screens/TabScreens/featured';
import Coupons from '../screens/TabScreens/coupons';
import Categories from '../screens/TabScreens/categories';
import More from '../screens/TabScreens/more';

//resource
import { AppTabActiveColor } from '../resource/commonStyles';

//Tab Images
import FavouritesTabImage from '../assets/ic_navFavorites.png';
import FeaturedTabImage from '../assets/ic_navFeatured.png';
import CouponsTabImage from '../assets/ic_couponBlue.png';
import CategoriesTabImage from '../assets/ic_navCategories.png';
import MoreTabImage from '../assets/ic_navMore.png';



/**
 * Tab bar of App.
 */
const AppNavigationTabs = createBottomTabNavigator({
    Favourites: {
        screen: Favourites,
        navigationOptions: {
            tabBarIcon: <Image source={FavouritesTabImage} style={{ width: 24, height: 24, resizeMode: 'contain' }} />,
        }
    },
    Featured: {
        screen: Featured,
        navigationOptions: {
            tabBarIcon: <Image source={FeaturedTabImage} style={{ width: 24, height: 24, resizeMode: 'contain' }} />,
        }
    },
    Coupons: {
        screen: Coupons,
        navigationOptions: {
            title: 'SHOP',
            tabBarIcon: <Image source={CouponsTabImage} style={{ width: 24, height: 24, resizeMode: 'contain' }} />,
        }
    },
    Categories: {
        screen: Categories,
        navigationOptions: {
            tabBarIcon: <Image source={CategoriesTabImage} style={{ width: 24, height: 24, resizeMode: 'contain' }} />,
        }
    },
    More: {
        screen: More,
        navigationOptions: {
            tabBarIcon: <Image source={MoreTabImage} style={{ width: 24, height: 24, resizeMode: 'contain' }} />,
        }
    }
}, {
    initialRouteName: 'Coupons',
    order: ['Favourites', 'Featured', 'Coupons', 'Categories', 'More'],
    tabBarOptions: {
        style: {
            backgroundColor: '#323232',
            alignItems: 'center',
            paddingTop: 12,
        },
        labelStyle: {
            fontSize: 12,
            paddingTop: 6
        },
        inactiveTintColor: 'white',
        activeTintColor: AppTabActiveColor,
    }
})

const AppNavigationRoute = createStackNavigator({
    AppNavigationTabs: AppNavigationTabs
}, {
    initialRouteName: 'AppNavigationTabs',
    headerMode: 'none',
    
})

export default createAppContainer(AppNavigationRoute);