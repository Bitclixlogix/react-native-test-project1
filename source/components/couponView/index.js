import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';

//resource
import { Style2, AppSecondaryColor } from '../../resource/commonStyles';

//Images
import couponLogo from '../../assets/ic_couponBlue.png';
import like from '../../assets/ic_Favorites.png';

const { width } = Dimensions.get('window');

export default class CouponCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        let { item } = this.props;
        return (
            <View style={[styles.cardContinerView, Style2.inCenter, Style2.borderStyle]}>
                <Image source={{ uri: item.image }} style={styles.image} />

                {this.renderItemContent(item)}

                {this.renderCouponLogo()}

                {this.renderLikeDislike()}
            </View>
        );
    }

    /************************* View function ********************************/

    /**
     * fucntion to render bottom item content.
     */
    renderItemContent = (item) => {
        return <View style={[Style2.positionAbsolute, { bottom: 12, alignItems: 'center' }]}>
            <Text style={{ fontSize: 16 }}>{item.price} lbs. /$</Text>
            <Text style={{ fontSize: 16, color: 'green' }}>{item.lbs} LBS</Text>
        </View>
    }

    /**
     * function to render like dislike heart button
     */
    renderLikeDislike = () => {
        return <View style={[Style2.positionAbsolute, { top: 12, left: 12 }]}>
            <Image source={like} style={{ width: 24, height: 24, resizeMode: 'contain' }} />
        </View>
    }

    /**
     * function to render coupon logo
     */
    renderCouponLogo = () => {
        return <View style={[Style2.positionAbsolute, { bottom: 12, right: 12 }]}>
            <Image source={couponLogo} style={{ width: 24, height: 24, resizeMode: 'contain' }} />
        </View>
    }
}

const styles = StyleSheet.create({
    cardContinerView: {
        width: (width / 2) - 32,
        height: (width / 2) - 32,
        padding: 16,
        margin: 8,
        borderColor: AppSecondaryColor
    },
    image: {
        height: 60,
        width: '100%',
        resizeMode: 'contain',
        marginBottom: 24
    }
})
