// Libraries
import React, { Component } from 'react';
import { View, Text, TextInput, Image, FlatList, StyleSheet } from 'react-native';

//Resource
import { Style2, AppSecondaryColor, AppAccentColor } from '../../../resource/commonStyles';

//Image
import search from '../../../assets/search.png';
import dropdown from '../../../assets/dropdown.png';
import threeDotsList from '../../../assets/ic_tileViewInactive_.png';
import twoDotsList from '../../../assets/ic_tileViewActiveBlue.png';
import list from '../../../assets/ic_listViewInactive.png';
import profile from '../../../assets/ic_user.png';

//Component
import CouponCard from './../../../components/couponView';

//dummy data.
const dummyData = [
    {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxCqpfNVXjP_HDzVOnbL6v4w2qe09pBqYjZ-lSeFA_QYYRwruE',
        price: '8',
        lbs: '10'
    },
    {
        image: 'http://mylastcoupon.com/wp-content/uploads/2016/06/AllPosters.com-Logo-CouponExit.png',
        price: '10',
        lbs: '30'
    },
    {
        image: 'https://pbs.twimg.com/profile_images/675342710065221632/mf1yk-VQ_400x400.png',
        price: '80',
        lbs: '112'
    },
    {
        image: 'https://upload.wikimedia.org/wikipedia/en/thumb/b/be/Booking.com_logo.svg/1280px-Booking.com_logo.svg.png',
        price: '500',
        lbs: '11'
    },
    {
        image: 'https://cdn.shopping.gives/merchant/chemical-guys_b7f96_20171114.png',
        price: '80-10',
        lbs: '43'
    },
    {
        image: 'https://static.chollometro.com/threads/thread_large/default/188956_1.jpg',
        price: '80',
        lbs: '200'
    },
    {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxCqpfNVXjP_HDzVOnbL6v4w2qe09pBqYjZ-lSeFA_QYYRwruE',
        price: '8',
        lbs: '10'
    },
    {
        image: 'http://mylastcoupon.com/wp-content/uploads/2016/06/AllPosters.com-Logo-CouponExit.png',
        price: '10',
        lbs: '30'
    },
    {
        image: 'https://pbs.twimg.com/profile_images/675342710065221632/mf1yk-VQ_400x400.png',
        price: '80',
        lbs: '112'
    },
    {
        image: 'https://upload.wikimedia.org/wikipedia/en/thumb/b/be/Booking.com_logo.svg/1280px-Booking.com_logo.svg.png',
        price: '500',
        lbs: '11'
    },
]


export default class Coupons extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: ''
        }
    }

    render() {
        return (
            <View style={[Style2.flex]}>
                {/* render top header  */}
                {this.renderHeader()}

                {/* render search field */}
                {this.renderSearchField()}

                {/* render dropdown and other listing button */}
                {this.renderContentView()}

                {/* render flatlist */}
                {this.renderList()}
            </View>
        );
    }

    /************************* View function ********************************/

    /**
     * fucntion to render upper header
     */
    renderHeader = () => {
        return <View style={[Style2.inRow, styles.headerView]}>
            <View style={[Style2.borderStyle, { borderColor: 'white', padding: 4 }]}>
                <Text style={{ color: 'white', fontSize: 12 }}>5,678 LSB CAPTURED</Text>
            </View>
            <Image source={profile} style={styles.listingImage} />
            <View style={[Style2.positionAbsolute, { width: '100%' }, Style2.inCenter]}>
                <Text style={{ color: 'white', fontSize: 16 }}>SHOP</Text>
            </View>
        </View>
    }

    /**
     * function to render search text feid.
     */
    renderSearchField = () => {
        let { searchText } = this.state;
        return <View style={[styles.searchBarView]}>
            <View style={[Style2.inRow, Style2.backgroundWhite, { borderRadius: 4, justifyContent: 'center' }]}>
                {searchText === '' && <Image source={search} style={{ height: 16, width: 16, resizeMode: 'contain' }} />}
                <TextInput
                    value={searchText}
                    onChangeText={searchText => this.setState({ searchText })}
                    style={[styles.searchTextInput]}
                    placeholder='Search'
                    placeholderTextColor={AppSecondaryColor}
                    underlineColorAndroid="transparent"
                />
            </View>
        </View>
    }

    /**
     * function to render dropdown and different listing type.
     */
    renderContentView = () => {
        return <View style={[Style2.inRow, { padding: 16, justifyContent: 'space-between' }]}>
            <View style={[Style2.inRow, Style2.borderStyle, styles.dropDown]}>
                <Text style={{ fontSize: 16 }}>Pounds Captured</Text>
                <Image source={dropdown} style={{ width: 10, height: 10, resizeMode: 'contain' }} />
            </View>
            <Image source={threeDotsList} style={styles.listingImage} />
            <Image source={twoDotsList} style={styles.listingImage} />
            <Image source={list} style={styles.listingImage} />
        </View>
    }

    /**
     * function to render list.
     */
    renderList = () => {
        return <View style={[Style2.flex, { paddingHorizontal: 16 }]}>
            <FlatList
                data={dummyData}
                numColumns={2}
                showsVerticalScrollIndicator={false}
                keyExtractor={(_, index) => index.toString()}
                extraData={this.state}
                renderItem={({ item, index }) => <CouponCard item={item} index={index} />}
            />
        </View>
    }
}

const styles = StyleSheet.create({
    searchBarView: {
        height: 48,
        padding: 8,
        backgroundColor: AppSecondaryColor
    },
    searchTextInput: {
        padding: 8,
        fontSize: 16,
        backgroundColor: 'white'
    },
    dropDown: {
        width: '55%',
        justifyContent: 'space-between',
        padding: 12,
        borderColor: AppSecondaryColor
    },
    listingImage: {
        width: 24,
        height: 24,
        resizeMode: 'contain'
    },
    headerView: {
        height: 56,
        justifyContent: 'space-between',
        backgroundColor: AppAccentColor,
        paddingHorizontal: 16
    }
})
