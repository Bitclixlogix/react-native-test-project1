// Libraries
import React, { Component } from 'react';
import { View, Text } from 'react-native';

//Resource
import { Style2 } from '../../../resource/commonStyles';

export default class Categories extends Component {
    render() {
        return (
            <View style={[Style2.flex, Style2.inCenter]}>
                <Text>This is Categories tab</Text>
            </View>
        );
    }
}
