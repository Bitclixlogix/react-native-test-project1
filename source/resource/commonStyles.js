import { Platform, StyleSheet } from 'react-native';

export const Style2 = StyleSheet.create({
    flex: {
        flex: 1
    },
    inCenter: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    inRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    backgroundWhite: {
        backgroundColor: '#FFFFFF'
    },
    positionAbsolute: {
        position: 'absolute'
    },
    shadow: {
        shadowColor: '#7F7F7F',
        shadowOffset: { width: 2, height: 4 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: Platform.OS === 'ios' ? 2 : 4,
    },
    borderStyle: {
        borderWidth: 1,
        borderRadius: 4
    }
});

export const AppAccentColor = '#3a76b3';

export const AppSecondaryColor = '#dcdcdc';

export const AppTabActiveColor = '#5487ba';